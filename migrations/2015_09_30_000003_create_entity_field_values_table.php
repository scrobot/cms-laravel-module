<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_field_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->string('entity_field_id');
            $table->text('value');
            $table->index('entity_id', 'entity_field_id');
            $table->foreign('entity_id')
                  ->references('id')->on('entities')
                  ->onDelete('cascade');
            $table->foreign('entity_field_id')
                  ->references('id')
                  ->on('entity_fields')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_field_values', function(Blueprint $table) {
            $table->dropForeign('entity_field_values_entity_field_id_foreign');
            $table->dropForeign('entity_field_values_entity_id_foreign');
        });
        Schema::drop('entity_field_values');
    }
}
