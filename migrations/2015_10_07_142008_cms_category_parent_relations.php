<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsCategoryParentRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_category_parent_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id');
            $table->unsignedInteger('child_id');
            $table->index('parent_id', 'child_id');
            $table->foreign('child_id')->references('id')->on('entity_categories');
            $table->foreign('parent_id')->references('id')->on('entity_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_category_parent_relations', function (Blueprint $table) {
            \Pinerp\Cms\Models\Category::where('id', ">", 0)->delete();
            $table->dropForeign('cms_category_parent_relations_parent_id_foreign');
            $table->dropForeign('cms_category_parent_relations_child_id_foreign');
        });
        Schema::drop('cms_category_parent_relations');
    }
}
