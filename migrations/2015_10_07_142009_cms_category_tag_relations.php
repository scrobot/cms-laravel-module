<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsCategoryTagRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_category_tag_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('binder_id');
            $table->index('tag_id', 'binder_id');
            $table->foreign('tag_id')->references('id')->on('entity_categories');
            $table->foreign('binder_id')->references('id')->on('entity_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_category_tag_relations', function (Blueprint $table) {
            \Pinerp\Cms\Models\Category::where('id', ">", 0)->delete();
            $table->dropForeign('cms_category_tag_relations_binder_id_foreign');
            $table->dropForeign('cms_category_tag_relations_tag_id_foreign');
        });
        Schema::drop('cms_category_tag_relations');
    }
}
