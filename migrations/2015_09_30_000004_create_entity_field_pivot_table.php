<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityFieldPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_categories_rel', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_category_id');
            $table->string('entity_field_id');
            $table->index('entity_category_id', 'entity_field_id');
            $table->foreign('entity_category_id')
                ->references('id')
                ->on('entity_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('entity_field_id')
                ->references('id')
                ->on('entity_fields')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_categories_rel', function(Blueprint $table) {
            $table->dropForeign('entity_categories_rel_entity_category_id_foreign');
            $table->dropForeign('entity_categories_rel_entity_field_id_foreign');
        });
        Schema::drop('entity_category_rel');
    }
}
