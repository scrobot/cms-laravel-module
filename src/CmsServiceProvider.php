<?php
namespace Pinerp\Cms;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;
use Pincommon\Layout\Migrator\MigratableTrait;

/**
 * Class PinCrmstaffServiceProvider
 * @package PinCrm/staff
 */
class CmsServiceProvider extends ServiceProvider
{

    use MigratableTrait;

    public static $pathToMigrations = '/vendor/pinerp/cms/migrations';

    public function boot()
    {
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../resources/lang'), 'cms');
        $this->loadViewsFrom(realpath(__DIR__ . '/../resources/views'), 'cms');

        /* publications */
        $this->publishes(
            [
                realpath(__DIR__ . '/../config') => base_path('config')
            ], 'config'
        );

        $this->publishes(
            [
                realpath(__DIR__ . '/../public') => public_path('cms'),
            ], 'public'
        );

        include __DIR__ . '/routes.php';

        viewper_push('left_menu', 'cms::left_menu', [], -1, 'main');

    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {

    }
}
