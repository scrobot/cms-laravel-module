<?php
namespace Pinerp\Cms;

use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Pincommon\Layout\BaseController;
use \Pinerp\Cms\Models\Value;
use Pinerp\Cms\Models\Category;
use Pinerp\Cms\Models\Entity;

/**
 * Class EntityController
 * @package Pinerp\Cms
 */
class EntityController extends BaseController
{
    /**
     * list all of entities
     * @param $cid // category id
     * @return mixed
     */
    public function getList($cid)
    {
        $category = Category::find($cid);

        $entities = Entity::ofCategory($category->id)->positionOrder()->get();

        return view('cms::entities.list')->withEntities($entities)->withCategory($category);
    }

    /**
     * showing entity content
     * @param $slug
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $entity = Entity::find($id);

        return view('cms::entities.show')->withEntity($entity)->withCategory($entity->category);
    }

    /**
     * @param $cid
     * @return mixed
     * @throws Exception
     */

    public function getCreate($cid)
    {
        $category = Category::find($cid);

        if(isset($category->options['is_single']) && count($category->entities) >= 1) {
            throw new Exception("Вы не можете создавать более одного сингла");
        }

        return view('cms::entities.create')->withEntity(null)->withCategory($category);
    }

    /**
     * @param Request $request
     * @param $cid
     * @return mixed
     * @throws Exception
     */
    public function postStore(Request $request, $cid)
    {
        $category = Category::find($cid);

        if(isset($category->options['is_single']) && count($category->entities) >= 1) {
            throw new Exception("Вы не можете создавать более одного сингла");
        }

        $entity = new Entity();
        $entity->category()->associate($category);
        $entity->save();

        foreach ($request->except('_token') as $id => $value) {
            (new Value)->saveValueWithRelations($id, $entity, $value);
        }

        return redirect(action('\Pinerp\Cms\EntityController@getEdit', $entity->id))->withMsg('layout::common.msg.create');
    }


    /**
     * editing entity
     * @param $cid
     * @param $id
     * @return mixed
     */
    public function getEdit($id)
    {
        $entity = Entity::find($id);

        //dd($entity->category->tags[0]->entities[0]->values);

        return view('cms::entities.edit')->withEntity($entity)->withCategory($entity->category);
    }

    /**
     * POST method of updating existing entity values or creating new if values would be not found
     * @param Request $request
     * @param $id
     * @return mixed
     */

    public function postUpdate(Request $request, $id)
    {
        $entity = Entity::find($id);

        foreach ($request->except('_token') as $id => $value) {
            Value::firstOrNew(['entity_field_id' => $id, 'entity_id' => $entity->id])->saveValueWithRelations($id, $entity, $value);
        }

        return back()->withMsg('layout::common.msg.edit');
    }


    /**
     * delete enitty with all value relations
     * @param $id
     * @return mixed
     */
    public function getDestroy($id)
    {
        Entity::destroy($id);

        return back()->withMsg('layout::common.msg.delete');
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function postSaveParent(Request $request, $id)
    {
        $entity = Entity::find($id);

        $entity->parents()->sync([$request->input('parent_id')]);

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function postSaveTags(Request $request, $id)
    {
        $entity = Entity::find($id);

        $entity->tags()->sync($request->input('tags'));

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function postSetWeight(Request $request)
    {
        foreach ($request->input('sorted_ids') as $orderValue => $id) {
            \DB::table($request->input('table'))->where('id', $id)->update([ 'position' => ($orderValue + 1) ]);
        }

        return $request->all();
    }
}