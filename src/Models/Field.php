<?php

namespace Pinerp\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class Field
 * @package Pinerp\Cms\Models
 */

class Field extends Model {

    protected $fillable = [];
    
    protected $guarded = [];
    
    protected $table = 'entity_fields';

    public $incrementing = false;

    public $timestamps = false;

    /**
     * relation many-to-many with pivot 'required' cell
     * @return $this
     */

    public function categories()
    {
        return $this->belongsToMany('\Pinerp\Cms\Models\Category', 'entity_categories_rel', 'entity_field_id', 'entity_category_id')->withPivot('required', 'weight');
    }

    public function values()
    {
        return $this->hasMany('\Pinerp\Cms\Models\Value', 'entity_field_id');
    }


    /**
     * id mutator
     * @param $value
     */

    public function setIdAttribute($value)
    {
        $this->attributes['id'] = \Stringy\StaticStringy::slugify($value, '_');
    }

    /**
     * Overload parent method "create"
     * @Illuminate\Database\Eloquent\Model::create(array $attributes = [])
     * @param array $attributes
     * @return static
     */

    public static function create(array $attributes = [])
    {
        $attributes['id'] = $attributes['name'];

        $field = parent::create($attributes);

        return $field;
    }

    /**
     * @param $name
     * @param $value
     * @param $attributes
     * @return \Illuminate\View\
     */

    public function getForm($name, $value, $attributes)
    {
        return view('cms::forms.' . $this->type, [ 'name' => $name, 'value' => $value, 'attributes' => $attributes, ]);
    }

    /**
     * query value from table "entity_field_values" by entity_id and entity_field_id parameters
     * @param $entity_id
     * @return null
     */
    public function valueFromEntity($entity_id)
    {
        $entity = Value::where('entity_id', $entity_id)->where('entity_field_id', $this->id)->first();
        if (is_null($entity)) {
            return null;
        } else {
            return $entity->value;
        }
    }
}
