<?php

namespace Pinerp\Cms\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Entity
 * @package Pinerp\Cms\Models
 */

class Entity extends Model {

    protected $fillable = [];
    
    protected $guarded = [];
    
    protected $table = 'entities';

    /**
     * Getter
     * Dynamically retrieve attributes on the model.
     *
     * @override parent::__get($key)
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        if(str_contains($key, 'the_')) {
            return $this->getEntityValueFromField(substr($key, 4));
        }

        return $this->getAttribute($key);
    }

    /**
     * @param $name
     * @return null
     */

    public function getEntityValueFromField($name)
    {
        try {
            return $this->category->fields->where('id', $name)->first()->values->where('entity_id', $this->id)->first()->value;
        } catch(\Exception $e) {
            return null;
        }
    }
    
    /**
     * Relation one-to-many with Category model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('\Pinerp\Cms\Models\Category', 'entity_category_id');
    }

    /**
     * Relation one-to-many with Value model
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany('\Pinerp\Cms\Models\Value');
    }

    public function children()
    {
        return $this->belongsToMany(static::class, 'cms_entity_parent_relations', 'parent_id', 'child_id');
    }

    public function parents()
    {
        return $this->belongsToMany(static::class, 'cms_entity_parent_relations', 'child_id', 'parent_id');
    }

    public function isParent($parent_id)
    {
        return $this->children->contains($parent_id);
    }

    public function tags()
    {
        return $this->belongsToMany(static::class, 'cms_entity_tags_relations', 'binder_id', 'tag_id');
    }

    public function tagger()
    {
        return $this->belongsToMany(static::class, 'cms_entity_tags_relations', 'tag_id', 'binder_id');
    }

    public function isBinder($binder_id)
    {
        return $this->tags->contains($binder_id);
    }

    public function scopePositionOrder($query)
    {
        return $query->orderBy('position');
    }

    public function scopeOfCategory($query, $cid)
    {
        return $query->where('entity_category_id', $cid);
    }

}
