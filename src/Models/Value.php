<?php

namespace Pinerp\Cms\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Value
 * @package Pinerp\Cms\Models
 */

class Value extends Model {

    protected $fillable = [];
    
    protected $guarded = [];
    
    protected $table = 'entity_field_values';

    public $timestamps = false;

    /**
     * relation one-to-many with Entity model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function entity()
    {
        return $this->belongsTo('\Pinerp\Cms\Models\Entity', 'entity_id');
    }

    /**
     * relation one-to-many with Field model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function field()
    {
        return $this->belongsTo('\Pinerp\Cms\Models\Field', 'id', 'entity_field_id');
    }

    /**
     * custom saving method for create and update controller methods
     * @param $field_id
     * @param Entity $entity
     * @param $value
     */
    public function saveValueWithRelations($field_id, Entity $entity, $value)
    {
        $this->entity_field_id = $field_id;
        $this->entity()->associate($entity);
        $this->value = $value;
        $this->save();
    }
    
}
