<?php

namespace Pinerp\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $fillable = [];

    protected $guarded = [];

    protected $table = 'entity_categories';

    protected $casts = [
        'options' => 'array',
    ];

    /**
     * relation one-to-many
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function entities()
    {
        return $this->hasMany('\Pinerp\Cms\Models\Entity', 'entity_category_id', 'id');
    }

    /**
     * relation many-to-many with pivot
     * @return $this
     */

    public function fields()
    {
        return $this->belongsToMany('\Pinerp\Cms\Models\Field', 'entity_categories_rel', 'entity_category_id', 'entity_field_id')->withPivot('required', 'weight');
    }

    /**
     * slug mutator
     * @param $value
     */

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = \Stringy\StaticStringy::slugify($value, '_');
    }

    /**
     * Attaching method - field to category with required pivot cell
     * @param $field_id
     * @param int $required
     * @return bool
     */
    
    public function attachWithRequired($field_id, $required = 0)
    {
        $this->fields()->attach($field_id, ['required' => $required]);

        return true;
    }

    /**
     * Overload parent method "create"
     * @Illuminate\Database\Eloquent\Model::create(array $attributes = [])
     * @param array $attributes
     * @return static
     */

    public static function create(array $attributes = [])
    {
        $attributes['slug'] = $attributes['name'];

        $category = parent::create($attributes);

        return $category;
    }

    /**
     * @param $field_id
     * @return bool
     */

    public function savePivotValue($field_id)
    {
        \DB::table('entity_categories_rel')->where('entity_field_id', $field_id)->where('entity_category_id', $this->id);
        \DB::table('entity_categories_rel')->where('entity_field_id', '!=', $field_id)->where('entity_category_id', $this->id);

        return true;
    }

    public static function singles()
    {
        $singles = collect();

        foreach(static::all() as $cat) {
            array_key_exists('is_single', is_null($cat->options) ? [] : $cat->options) ? $singles->push($cat) : null;
        }

        return $singles;

    }

    public static function categories()
    {
        $categories = collect();

        foreach(static::all() as $cat) {
            !array_key_exists('is_single', is_null($cat->options) ? [] : $cat->options) ? $categories->push($cat) : null;
        }

        return $categories;
    }

    public function single()
    {
        if(count($this->entities))
            return $this->entities->first();

        abort(404);
    }

    public function allExceptThis()
    {
        return static::where('id', '!=', $this->id)->get();
    }

    public function children()
    {
        return $this->belongsToMany(static::class, 'cms_category_parent_relations', 'parent_id', 'child_id');
    }

    public function parents()
    {
        return $this->belongsToMany(static::class, 'cms_category_parent_relations', 'child_id', 'parent_id');
    }
    
    public function parent()
    {
        if(!isset($this->parents[0])) {
            return null;
        }

        return $this->parents[0];
    }

    public function isParent($parent_id)
    {
        return $this->children->contains($parent_id);
    }

    public function tags()
    {
        return $this->belongsToMany(static::class, 'cms_category_tag_relations', 'binder_id', 'tag_id');
    }

    public function tagger()
    {
        return $this->belongsToMany(static::class, 'cms_category_tag_relations', 'tag_id', 'binder_id');
    }

    public function isBinder($binder_id)
    {
        return $this->tags->contains($binder_id);
    }

    public function safeAsParent()
    {
        $categories = $this->allExceptThis();
        $ids = [];

        foreach ($categories as $category) {
            if(!$this->tags->contains($category->id)) {
                $ids[] = $category->id;
            }
        }

        foreach ($ids as $key => $value) {
            if($this->children->contains($value)) {
                $ids = array_except($ids, [$key]);
            }
        }

        if(empty($ids)) {
            return $categories;
        }

        return static::whereIn('id', $ids)->get();

    }

    public function safeAsTags()
    {
        $categories = $this->allExceptThis();
        $ids = [];

        foreach ($categories as $category) {
            if(!$category->children->contains($this->id)) {
                $ids[] = $category->id;
            }

        }

        foreach ($ids as $key => $value) {
            if($this->children->contains($value)) {
                $ids = array_except($ids, [$key]);
            }
        }

        if(empty($ids)) {
            return $categories;
        }

        return static::whereIn('id', $ids);
    }

    public function entityByField($field_name, $field_value)
    {
        return $this->fields
            ->where('id', $field_name)
            ->first()
            ->values
            ->where('value', $field_value)
            ->where('entity_field_id', $field_name)
            ->first()
            ->entity;
    }
}