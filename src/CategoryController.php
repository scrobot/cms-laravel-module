<?php

namespace Pinerp\Cms;

use Pincommon\Layout\BaseController;
use Pinerp\Cms\Models\Category;
use Pinerp\Cms\Models\Field;
use Pinerp\Cms\Requests\CategoryPostRequest;
use Pinerp\Cms\Requests\CreateAndAttachFieldPostRequest;
use Illuminate\Http\Request;

/**
 * Class CategoryController
 * @package Pinerp\Cms
 */

class CategoryController extends BaseController
{

    /**
     * all categories list
     * @return mixed
     */

    public function getIndex()
    {
        $categories = Category::all();

        return view('cms::categories.list')->withCategories($categories);
    }

    /**
     * show category info
     * @param $id
     */

    public function getShow($id)
    {

    }

    /**
     * create an entity category
     * @return \Illuminate\View\View
     */

    public function getCreate()
    {
        return view('cms::categories.create');
    }

    /**
     * POST method of creating the entity category
     * @param CategoryPostRequest $request
     * @return mixed
     */

    public function postStore(CategoryPostRequest $request)
    {
        $input = $request->all();

        $category = Category::create($input);

        return redirect(action('\Pinerp\Cms\CategoryController@getEdit', $category->id))->withMsg('layout::common.msg.create');
    }

    /**
     * editing the entity category
     * @param $id
     * @return mixed
     */

    public function getEdit($id)
    {

        $category = Category::find($id);

        $fields = Field::all();

        return view('cms::categories.edit')->withCategory($category)->withFields($fields);
    }

    /**
     * POST method of updating existing entity category
     * @param CategoryPostRequest $request
     * @param $id
     * @return mixed
     */
    
    public function postUpdate(CategoryPostRequest $request, $id)
    {

        Category::find($id)->update($request->all());

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * deleting the entity category
     * @param $id
     * @return mixed
     */

    public function getDestroy($id)
    {
        $category = Category::find($id);

        $category->delete();

        return back()->withMsg('layout::common.msg.delete');
    }

    /**
     * @param CreateAndAttachFieldPostRequest $request
     * @param $cat_id
     * @return mixed
     */

    public function postCreateAndAttachField(CreateAndAttachFieldPostRequest $request, $cat_id)
    {
        $category = Category::find($cat_id);

        //receive $request for creating new field
        $field = Field::create($request->all());

        //attaching new field to current category
        $category->attachWithRequired($field->id, is_null($request->input('required')) ? 0 : 1);

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * Attaching existing field
     * @param Request $request
     * @param $cat_id
     * @return mixed
     */

    public function postAttachField(Request $request, $cat_id)
    {
        $category = Category::find($cat_id);

        //attaching new field to current category
        $category->attachWithRequired($request->input('field_id'));

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * Detaching existing field
     * @param $cat_id
     * @param $field_id
     * @return mixed
     */

    public function getDetachField($cat_id, $field_id)
    {
        $category = Category::find($cat_id);

        $category->fields()->detach($field_id);

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * @param Request $request
     * @return array
     */

    public function postSetWeight(Request $request)
    {
        foreach ($request->input('sorted_ids') as $orderValue => $id) {
            \DB::table($request->input('table'))->where('entity_category_id', $request->input('category'))->where('entity_field_id', $id)->update([ 'weight' => ($orderValue + 1) ]);
        }

        return $request->all();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */

    public function postSetParents(Request $request, $id)
    {

        $category = Category::find($id);
        $category->parents()->sync([$request->input('rel')]);

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */

    public function postSetTags(Request $request, $id)
    {

        $category = Category::find($id);
        $category->tags()->sync($request->input('rel'));

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */

    public function postOptions(Request $request, $id)
    {
        $category = Category::find($id);
        $category->options = $request->input('options');

        $category->save();

        return back()->withMsg('layout::common.msg.edit');
    }

    /**
     * @param Request $request
     * @param $id
     * @return int
     */
    public function postPivotTitle (Request $request, $id) 
    {
        $category = Category::find($id);
        $category->savePivotValue($request->input('field'));

        return 1;
    }

    public function postEditAttachedFields(Request $request)
    {
        if (!is_null($request->input('field'))) {
            foreach ($request->input('field') as $field_id => $attributes) {
                $field = Field::find($field_id);
                $field->update($attributes);
            }
        } else {
            return back()->withErrors(trans('cms::trans.errors.no_attached_fields'));
        }

        return back()->withMsg('layout::common.msg.edit');

    }

    

}