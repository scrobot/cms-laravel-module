<?php

return [

    'field_types' => [
        'text' => 'Строчное поле',
        'textarea' => 'Текстовое поле',
        'ckeditor' => 'CKEditor',
    ]

];
