<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-pills">
            <li @lightAction('\Pinerp\Cms\CategoryController@getIndex')><a href="{{action('\Pinerp\Cms\CategoryController@getIndex')}}">{{trans('cms::trans.all_categories')}}</a></li>
            <li @lightAction('\Pinerp\Cms\CategoryController@getCreate')><a href="{{action('\Pinerp\Cms\CategoryController@getCreate')}}">{{trans('cms::trans.create_category')}}</a></li>
        </ul>
    </div>
</div>