<li @lightAction('\Pinerp\Cms\CategoryController')>
    <a href="{{action('\Pinerp\Cms\CategoryController@getIndex')}}">{{trans('cms::trans.name')}}</a>
    <h5>Категории</h5>
    <ul>
        @foreach(\Pinerp\Cms\Models\Category::categories() as $category)
            <li><a href="{{action('\Pinerp\Cms\EntityController@getList', $category->id)}}">{{$category->name}}</a></li>
        @endforeach
    </ul>
    <h5>Синглы</h5>
    <ul>
        @foreach(\Pinerp\Cms\Models\Category::singles() as $category)
            <li><a href="{{action('\Pinerp\Cms\EntityController@getList', $category->id)}}">{{$category->name}}</a></li>
        @endforeach
    </ul>
</li>
