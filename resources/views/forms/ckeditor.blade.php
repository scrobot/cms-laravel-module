{!! Form::textarea($name, $value, ['class' => 'form-control', 'rows' => '5'] + $attributes) !!}
<script>
    CKEDITOR.replace( '{{$attributes['id']}}' );
</script>