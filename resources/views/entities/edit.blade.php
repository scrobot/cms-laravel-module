@extends("layout::layout")

@section('head')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            var ordering = $('#fields'), cont = ordering.children('div');

            cont.detach().sort(function(a, b) {
                var astts = $(a).data('weight');
                var bstts = $(b).data('weight')
                return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
            });

            ordering.append(cont)
        })
    </script>
    <script type="text/javascript" src="/cms/ckeditor/ckeditor.js"></script>
@stop

@section("breadcrumbs")
    @include('cms::entities.actions', ['name' => $category->name, 'id' => $category->id])
@stop

@section("content")



    <h1>{{trans('cms::trans.templates.entities.edit', ['entity' => $category->name])}}</h1>

    <div class="row">
        <div class="col-md-6">
            {!! \Form::model($entity, ['action' => ['\Pinerp\Cms\EntityController@postUpdate', $entity->id]]) !!}
            <div id="fields">
                @foreach($category->fields as $field)
                    <div class="form-group" data-weight="{{$field->pivot->weight}}">
                        <label for="input-{{$field->id}}">{{$field->name}}</label>
                        {!! $field->getForm($field->id, $field->valueFromEntity($entity->id), ['class' => 'form-control', 'placeholder' => $field->name, 'id' => "input-{$field->id}"]) !!}
                    </div>
                @endforeach
            </div>

            <button class="btn btn-lg btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
            {!! \Form::close() !!}
        </div>
    </div>

    @unless(is_null($category->parent()))
        <h2>{{$category->parent()->name}}</h2>

        <div class="row">
            <div class="col-md-6">
                {!! \Form::open(['action' => ['\Pinerp\Cms\EntityController@postSaveParent', $entity->id]]) !!}
                @foreach($category->parent()->entities as $parent)
                <div class="form-group">
                    {!! \Form::radio('parent_id', $parent->values[0]->entity_id, $parent->isParent($entity->id), ['id' => "input-".$parent->id]); !!}
                    <label for="input-{{$parent->id}}">{{$parent->values[0]->value or "{$category->parent()->id}_{$parent->id}"}}</label>
                </div>
                @endforeach
                <button class="btn btn-lg btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
                {!! \Form::close() !!}
            </div>
        </div>
    @endunless

    @if(is_null($category->tags))
        {!! \Form::open(['action' => ['\Pinerp\Cms\EntityController@postSaveTags', $entity->id]]) !!}
        @foreach($category->tags as $tag)
            <h2>{{$tag->name}}</h2>
            <div class="row">
                <div class="col-md-6">
                    @foreach($tag->entities as $tag_entity)
                        <div class="form-group">
                            {!! \Form::checkbox('tags[]', $tag_entity->values[0]->entity_id, $entity->isBinder($tag_entity->id), ['id' => "input-".$tag_entity->id]); !!}
                            <label for="input-{{$tag_entity->id}}">{{empty($tag_entity->values[0]->value) ? "{$tag->name}_{$tag_entity->id}" : $tag_entity->values[0]->value}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-lg btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
            </div>
        </div>
        {!! \Form::close() !!}
    @endif
@stop
