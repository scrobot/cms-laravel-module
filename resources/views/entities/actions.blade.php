<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-pills">
            <li @lightAction('\Pinerp\Cms\EntityController@getList')><a href="{{action('\Pinerp\Cms\EntityController@getList', $id)}}">{{trans('cms::trans.templates.entities.all', ['entity' => $name])}}</a></li>
            <li @lightAction('\Pinerp\Cms\EntityController@getCreate')><a href="{{action('\Pinerp\Cms\EntityController@getCreate', $id)}}">{{trans('cms::trans.templates.entities.create', ['entity' => $name])}}</a></li>
        </ul>
    </div>
</div>