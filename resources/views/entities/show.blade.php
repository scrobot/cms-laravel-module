@extends("layout::layout")

@section('head')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <script type="text/javascript" src="/cms/ckeditor/ckeditor.js"></script>
@stop

@section("breadcrumbs")
    @include('cms::entities.actions', ['name' => $category->name, 'id' => $category->id])
@stop

@section("content")



    <h1>{{trans('cms::trans.templates.entities.see')}}</h1>

    <div class="row">
        <div class="col-md-8">
            @foreach($entity->values as $value)
                <div class="col-xs-12">
                    <h3>{{$value->field->name}}</h3>
                    <div class="content">
                        {!!$value->value!!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@stop
