@extends("layout::layout")

@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            var ordering = $('#sortable tbody'), cont = ordering.children('tr');

            cont.detach().sort(function(a, b) {
                var astts = $(a).data('weight');
                var bstts = $(b).data('weight')
                return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
            });

            ordering.append(cont);

            $('td, th', '#sortable').each(function () {
                var cell = $(this);
                cell.width(cell.width());
            });

            var sort = $("#sortable tbody");
            var axis = sort.attr('axis');

            sort.sortable({

                items: "> tr",
                cursor: "move",
                axis: axis,
                revert: 100,

                start: function (event, ui) {
                    var cellCount = 0;
                    $('td, th', ui.helper).each(function () {
                        var colspan = 1;
                        var colspanAttr = $(this).attr('colspan');
                        if (colspanAttr > 1) {
                            colspan = colspanAttr;
                        }
                        cellCount += colspan;
                    });

                    ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');

                    var sorting = new Array();
                    var i = 0;

                },

                update: function(event, ui) {

                    // alert(1);

                    var sortedArr = sort.sortable("toArray", {
                        attribute: "object-id"
                    });

                    //alert(sortedArr);

                    $.post("/erp/cms/entity/set-weight", {sorted_ids: sortedArr, table: 'entities'}, function (response) {
                        console.log(response)
                    }).fail(function (x) { alert(x.responseText)});


                }

            }).disableSelection();

        })
    </script>
@stop

@section("breadcrumbs")
    @include('cms::entities.actions', ['name' => $category->name, 'id' => $category->id])
@stop

@section("content")

    <h1>{{$category->name}}</h1>

    <div class="row">
        <div class="col-md-6">
            <table id="{{isset($category->options['sortable']) ? "sortable" : ""}}" class="table table-striped table-bordered">
                <tbody>
                @forelse($entities as $entity)
                    <tr object-id="{{$entity->id}}" data-weight="{{$entity->position}}">
                        <td>
                            @if(isset($category->options['sortable']))
                                <i class="glyphicon glyphicon-move"></i>
                            @endif
                            <a href="{{action('\Pinerp\Cms\EntityController@getShow', $entity->id)}}">{{$entity->values[0]->value}}</a>
                        </td>
                        <td>
                            <a href="{{action('\Pinerp\Cms\EntityController@getEdit', $entity->id)}}" data-toggle="tooltip" data-placement="top" title="{{trans('layout::common.button.edit')}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{action('\Pinerp\Cms\EntityController@getDestroy', $entity->id)}}" data-toggle="tooltip" data-placement="top" title="{{trans('layout::common.button.delete')}}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">
                            {{trans('cms::trans.404.entities', ['cat' => $category->name])}}
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

@stop
