@extends("layout::layout")

@section('head')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            var ordering = $('#fields'), cont = ordering.children('div');

            cont.detach().sort(function(a, b) {
                var astts = $(a).data('weight');
                var bstts = $(b).data('weight')
                return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
            });

            ordering.append(cont)
        })
    </script>
    <script type="text/javascript" src="/cms/ckeditor/ckeditor.js"></script>
@stop

@section("breadcrumbs")
    @include('cms::entities.actions', ['name' => $category->name, 'id' => $category->id])
@stop

@section("content")



    <h1>{{trans('cms::trans.templates.entities.create', ['entity' => $category->name])}}</h1>

    <div class="row">
        <div class="col-md-6">
            {!! \Form::model($entity, ['action' => ['\Pinerp\Cms\EntityController@postStore', $category->id]]) !!}

            <div id="fields">
                @foreach($category->fields as $field)
                    <div class="form-group" data-weight="{{$field->pivot->weight}}">
                        <label for="input-{{$field->id}}">{{$field->name}}</label>
                        {!! $field->getForm($field->id, null, ['class' => 'form-control', 'placeholder' => $field->name, 'id' => "input-{$field->id}"]) !!}
                    </div>
                @endforeach
            </div>

            <button class="btn btn-lg btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
            {!! \Form::close() !!}
        </div>
    </div>

@stop
