@extends("layout::layout")

@section('head')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@stop

@section("breadcrumbs")
    @include('cms::actions_menu')
@stop

@section("content")



    <h1>{{trans('cms::trans.templates.categories.parent', ['cat' => $category->name])}}</h1>

    <div class="row">
        <div class="col-md-6">
            {!! \Form::open(['action' => ['\Pinerp\Cms\CategoryController@postParents', $category->id]]) !!}

                @foreach($related as $cat)
                <div class="form-group">
                    {!! \Form::radio('parent', $cat->id, $category->selfRel->contains($cat->id), ['id' => 'parent-'.$cat->id]); !!}
                    {!! \Form::label('parent-'.$cat->id, $cat->name)!!}
                </div>
                @endforeach

                <button class="btn btn-lg btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
            {!! \Form::close() !!}
        </div>
    </div>

@stop
