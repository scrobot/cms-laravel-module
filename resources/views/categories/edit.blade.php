@extends("layout::layout")

@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            var ordering = $('#sortable tbody'), cont = ordering.children('tr');

            cont.detach().sort(function(a, b) {
                var astts = $(a).data('weight');
                var bstts = $(b).data('weight')
                return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
            });

            ordering.append(cont);

            $('td, th', '#sortable').each(function () {
                var cell = $(this);
                cell.width(cell.width());
            });

            var sort = $("#sortable tbody");
            var axis = sort.attr('axis');

            sort.sortable({

                items: "> tr",
                cursor: "move",
                axis: axis,
                revert: 100,

                start: function (event, ui) {
                    var cellCount = 0;
                    $('td, th', ui.helper).each(function () {
                        var colspan = 1;
                        var colspanAttr = $(this).attr('colspan');
                        if (colspanAttr > 1) {
                            colspan = colspanAttr;
                        }
                        cellCount += colspan;
                    });

                    ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');

                    var sorting = new Array();
                    var i = 0;

                },

                update: function(event, ui) {

                    // alert(1);

                    var sortedArr = sort.sortable("toArray", {
                        attribute: "object-id"
                    });

                    var categoryId = "{{$category->id}}";

                    // alert(sortedArr);

                    $.post("/erp/cms/category/set-weight", {sorted_ids: sortedArr, category: categoryId, table: 'entity_categories_rel'}, function (response) {
                        console.log(response)
                    }).fail(function (x) { alert(x.responseText)});


                }

            }).disableSelection();

            $(document).on('click', '#is_title', function(){

                $.post("{{action('\Pinerp\Cms\CategoryController@postPivotTitle', $category->id)}}", {field: $(this).val()}, function(response) {
                    if(response == 1) {
                        console.log('success')
                    }
                })

            })

        })
    </script>
@stop

@section("breadcrumbs")
    @include('cms::actions_menu')
@stop

@section("content")

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">{{$category->name}}</h2>
                </div>

                <div class="panel-body">

                    {{--edit category attrs--}}
                    {!! \Form::model($category, ['class' => 'form-inline', 'action' => ['\Pinerp\Cms\CategoryController@postUpdate', $category->id]]) !!}
                        <div class="form-group">

                            <label for="name">
                                {{trans('cms::trans.templates.categories.name')}}<span style="color:red">*</span>
                            </label>

                            {!! \Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('cms::trans.templates.categories.name'), 'required', 'autofocus']) !!}
                        </div>

                        <button class="btn btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>

                    {!! \Form::close() !!}

                    <br/>

                    {{--/edit category attrs--}}

                    {!! \Form::open(['class' => 'form-inline well', 'action' => '\Pinerp\Cms\CategoryController@postEditAttachedFields']) !!}
                        <table id="sortable" class="table table-striped table-bordered">
                            <thead>
                                <tr class="head">
                                    <th></th>
                                    <th>{{trans('staff::staff.table.name')}}</th>
                                    <th>{{trans('staff::staff.table.type')}}</th>
                                    <th>{{trans('cms::trans.templates.categories.slug')}}</th>
                                    <th>{{trans('staff::staff.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($category->fields as $field)
                                <tr object-id="{{$field->id}}" data-weight="{{$field->pivot->weight}}">
                                    <td class="text-center"><i class="glyphicon glyphicon-resize-vertical"></i></td>
                                    <td>{!! \Form::text("field[{$field->id}][name]", $field->name, ['class' => 'form-control', 'required', 'autofocus']) !!}</td>
                                    <td>{!! \Form::text("field[{$field->id}][id]", $field->id, ['class' => 'form-control', 'required', 'autofocus']) !!}</td>
                                    <td>{!! Form::select("field[{$field->id}][type]", config('cms.field_types'), $field->type, ['class' => 'form-control']) !!}</td>
                                    <td><a href="{{action('\Pinerp\Cms\CategoryController@getDetachField', [$category->id, $field->id])}}"
                                           class="btn btn-danger btn-xs">{{trans('layout::common.button.detach')}}</a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" style="text-align: center">{{trans('staff::staff.no_attached_fields')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <button class="btn btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
                    {!! \Form::close() !!}

                    {!! \Form::open(['class' => 'form-inline well', 'action' => ['\Pinerp\Cms\CategoryController@postCreateAndAttachField', $category->id]]) !!}
                    <div class="form-group">
                        <input class="form-control" placeholder="{{trans('staff::staff.naming')}}" name="name" type="text">
                    </div>
                    <div class="form-group">
                        {!! Form::select('type', config('cms.field_types'), null, ['class' => 'form-control']) !!}
                    </div>
                    <button class="btn btn-primary" type="submit">{{trans('cms::trans.templates.categories.add_field')}}</button>
                    {!! \Form::close() !!}

                    @if(count($category->safeAsParent()) && count($category->safeAsTags()))

                    @unless(isset($category->options['is_single']))
                    {!! \Form::open(['class' => 'form-inline well', 'action' => ['\Pinerp\Cms\CategoryController@postSetParents', $category->id]]) !!}
                        <p>Выберите, для какой категории "{{$category->name}}" будет дочерней</p>
                        <div class="form-group">
                            <select class="form-control" name="rel">
                                <option value="0">Ни для какой</option>
                                @foreach($category->safeAsParent() as $cat)
                                    <option value="{{$cat->id}}" {{$cat->isParent($category->id) ? 'selected' : false}}>{{$cat->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
                    {!! \Form::close() !!}
                    @endunless

                    @unless(isset($category->options['is_single']))
                    {!! \Form::open(['class' => 'form-inline well', 'action' => ['\Pinerp\Cms\CategoryController@postSetTags', $category->id]]) !!}
                        <p>Выберите теги для категории "{{$category->name}}"</p>
                        <div class="form-group">
                            @foreach($category->safeAsTags()->get() as $cat)
                                <div class="checkbox">
                                    <label>
                                        <input name="rel[]" type="checkbox" value="{{$cat->id}}" {{$category->tags->contains($cat->id) ? 'checked' : false}}> {{$cat->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <button class="btn btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>
                    {!! \Form::close() !!}
                    @endunless

                    @endif
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">Дополнительные опции категории "{{$category->name}}"</h2>
                        </div>

                        <div class="panel-body">
                            {!! \Form::open(['class' => 'form', 'action' => ['\Pinerp\Cms\CategoryController@postOptions', $category->id]]) !!}

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input name="options[sortable]" type="checkbox" value="checked" {{isset($category->options['sortable']) ? $category->options['sortable'] : ""}}> {{trans('cms::trans.templates.categories.sortable')}}
                                    </label>
                                </div>
                            </div>
                            @if(count($category->fields))
                            <h4>{{trans('cms::trans.templates.categories.use_as_title')}}</h4>
                            <div class="form-group">
                                @foreach($category->fields as $field)
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="options[title]" id="field-{{$field->id}}" value="{{$field->id}}" {{isset($category->options['title']) && $category->options['title'] == $field->id ? "checked" : ''}}>
                                        {{$field->name}}
                                    </label>
                                </div>
                                @endforeach
                            </div>
                            @endif
                            @if(count($category->entities) <= 1)
                                <h4>{{trans('cms::trans.templates.categories.is_single')}}</h4>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input name="options[is_single]" type="checkbox" value="checked" {{isset($category->options['is_single']) ? $category->options['is_single'] : ""}}> {{trans('cms::trans.templates.categories.true')}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                            <button class="btn btn-primary" type="submit">{{trans('layout::common.button.save')}}</button>

                            {!! \Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
