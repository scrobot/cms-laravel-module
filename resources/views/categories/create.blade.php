@extends("layout::layout")

@section('head')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@stop

@section("breadcrumbs")
    @include('cms::actions_menu')
@stop

@section("content")



    <h1>{{trans('cms::trans.create_category')}}</h1>

    <div class="row">
        <div class="col-md-6">
            {!! \Form::open(['action' => '\Pinerp\Cms\CategoryController@postStore']) !!}

                <div class="form-group">
                    <label for="name">{{trans('cms::trans.templates.categories.name')}}<span style="color:red">*</span></label>
                    {!! \Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('cms::trans.templates.categories.name'), 'required', 'autofocus']) !!}
                </div>

                <h4>{{trans('cms::trans.templates.categories.is_single')}}</h4>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input name="options[is_single]" type="checkbox" value="checked"> {{trans('cms::trans.templates.categories.true')}}
                        </label>
                    </div>
                </div>

                <button class="btn btn-lg btn-primary" type="submit">{{trans('layout::common.button.create')}}</button>
            {!! \Form::close() !!}
        </div>
    </div>

@stop
