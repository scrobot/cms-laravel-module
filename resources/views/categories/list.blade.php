@extends("layout::layout")

@section('head')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@stop

@section("breadcrumbs")
    @include('cms::actions_menu')
@stop

@section("content")



    <h1>{{trans('cms::trans.categories')}}

    </h1>

    <div class="row">
        <div class="col-md-6">
            <table class="table table-striped table-bordered">
                <tbody>
                @forelse($categories as $category)
                    <tr>
                        <td><a href="{{action('\Pinerp\Cms\CategoryController@getShow', $category->id)}}">{{$category->name}}</a></td>
                        <td>
                            <a href="{{action('\Pinerp\Cms\CategoryController@getEdit', $category->id)}}" data-toggle="tooltip" data-placement="top" title="{{trans('layout::common.button.edit')}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{action('\Pinerp\Cms\CategoryController@getDestroy', $category->id)}}" data-toggle="tooltip" data-placement="top" title="{{trans('layout::common.button.delete')}}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">
                            {{trans('cms::trans.404.categories')}}
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

@stop
